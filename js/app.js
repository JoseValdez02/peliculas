document.getElementById('btnBuscar').addEventListener('click', function() {
    // Obtener el valor del título de la película desde la caja de texto
    const tituloPelicula = document.getElementById('inputTitulo').value;

    // Construir la URL del API con el título de la película proporcionado
    const url = `https://www.omdbapi.com/?t=${tituloPelicula}&plot=full&apikey=30063268`;

    // Hacer la solicitud Fetch para obtener los datos de la película
    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw new Error('La solicitud al servidor falló');
        }
        return response.json();
      })
      .then(data => {
        // Verificar si el título ingresado coincide exactamente con el título devuelto por el API
        if (data.Title.toLowerCase() === tituloPelicula.toLowerCase()) {
            // Mostrar los datos obtenidos en la página web
            document.getElementById('nombrePelicula').textContent = data.Title;
            document.getElementById('añoCreacion').textContent = data.Year;
            document.getElementById('actores').textContent = data.Actors;
            document.getElementById('reseña').textContent = data.Plot;
            document.getElementById('imagen').src = data.Poster;
            document.getElementById('imagen').alt = `Imagen de ${data.Title}`;
            document.getElementById('resultado').style.display = 'block';
        } else {
            alert('El título ingresado no coincide con ninguna película encontrada.');
        }
      })
      .catch(error => console.log('Error al obtener los datos de la película:', error.message));
});
